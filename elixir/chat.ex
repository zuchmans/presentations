defmodule Chat.Client do
  # In all of the following functions 'server' stands for the server's pid
  def join(server) do
    send_message server, :join
  end

  def say(server, message) do
    send_message server, { :say, message }
  end

  def leave(server) do
    send_message server, :leave
  end

  # Receive a pending message from 'server' and print it
  def flush(server) do
    receive do
      # The caret '^' is used to match against the value of 'server',
      # it is a basic filtering based on the sender
      { ^server, {:message, message} } ->
        IO.puts message
    end
  end

  # Send 'message' to 'server' and wait for a reply
  # Notice how this function is declared using 'defp' meaning
  # it's private and can only be called inside this module
  defp send_message(server, message) do
    send server, { Process.self, message }
    receive do
      { ^server, response } ->
        response
    after
      1000 ->
        IO.puts "Connection to room timed out"
        :timeout
    end
  end
end

defmodule Chat.Server do
  # A Room record acts as a reference to 'this' or 'self' in other langs
  defrecord Room, clients: []

  # The main receive loop
  def msg_loop(room) do
    receive do
      {pid, :join } ->
        notify_all room, Process.self, "Some user with pid #{inspect pid} joined"
        send pid, { Process.self, :ok }

        room = room.clients([pid | room.clients])
        msg_loop room

      {pid, {:say, message}} ->
        notify_all room, pid, "#{inspect pid}: " <> message
        send pid, { Process.self, :ok }
        msg_loop room

      {pid, :leave} ->
        send pid, { Process.self, :ok }
        # 'update_clients' is another automatically generated function
        new_room = room.update_clients(fn(clients) -> List.delete(clients, pid) end)
        notify_all new_room, Process.self, "User with pid #{inspect pid} left"
        msg_loop new_room
    end
  end

  # Send 'message' to all clients except 'sender'
  defp notify_all({Room, clients}, sender, message) do
    Enum.each clients, fn(pid) ->
      if pid != sender do
        send pid, { Process.self, {:message, message} }
      end
    end
  end
end

# Init a Room record with the current process
room = Chat.Server.Room.new(clients: [Process.self])

# Spawn a server process
server_pid = spawn fn() -> Chat.Server.msg_loop room end

# Spawn another process as client
spawn fn() ->
  Chat.Client.join server_pid
  Chat.Client.say server_pid, "Hi!"
  Chat.Client.leave server_pid
end

# # And another one
spawn fn() ->
  Chat.Client.join server_pid
  Chat.Client.say server_pid, "What's up?"
  Chat.Client.leave server_pid
end

# # By this time we have 6 notifications pending
# # (corresponding to three 'notify_all' calls per each client)
Enum.each 1..6, fn(_) ->
  Chat.Client.flush server_pid
end